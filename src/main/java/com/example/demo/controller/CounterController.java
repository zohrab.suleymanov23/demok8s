package com.example.demo.controller;

import com.example.demo.CounterService;
import com.example.demo.model.Counter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class CounterController {

    private final CounterService service;


    @GetMapping("/greeting")
    public String sayHello() {



        return "Hello Zohrab time is " + LocalDateTime.now();
    }

    @GetMapping("/counter")
    public List<Counter> getAll(HttpServletRequest request) throws Exception{

        InetAddress inetAddress=InetAddress.getLocalHost();
        Counter counter=new Counter();
        counter.setInfo(LocalDateTime.now());
        counter.setHost(inetAddress.getHostName());
        counter.setRequestPath(request.getRequestURI());

        service.save(counter);

        return service.getCounters();
    }

    @DeleteMapping("delete-all")
    public void deleteAll(){

        service.deleteAll();
    }






}
