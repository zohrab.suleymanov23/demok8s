package com.example.demo;

import com.example.demo.model.Counter;
import com.example.demo.repository.CounterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class CounterService {


    @Autowired
   private CounterRepository repository;


    public List<Counter> getCounters(){

        return repository.findAll();
    }

    public void save(Counter counter){


        repository.save(counter);

    }

    public  void  deleteAll(){

        repository.deleteAll();
    }


}
